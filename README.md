# HelloWorldAngular5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.5.

## Requirements

### NodeJS and NPM

In this project, I am using NodeJS 8.9

```shell
$ sudo apt-get install node
$ sudo apt-get install npm
```

## Installation of dependencies

Each time you download a new NodeJS project, you MUST install the dependencies (that means you don't need to install everytime, only once or when you add a new package).

```shell
$ npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
